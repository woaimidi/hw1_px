#!/usr/bin/ruby

def array_uppercase(*input)
	output = Array.new(input.length)
   
   for i in 0...input.length
      output[i] = input[i].upcase
   end

   return output
end

# puts array_uppercase "abc", "edc", "rfv"

def sum(*input)
	output = 0
   for i in 0...input.length
      output = output + input[i]
   end


   return output
end

# puts sum 1,2,3,4,5,6

class Student
	attr_reader :name, :age, :program, :major

	def initialize(name, age, program, major)  
	    # Instance variables  
	    @name, @age, @program, @major = name, age, program, major
	    # @name = name
	    # @age = age
	    # @program = program
	    # @major = major
	end 

	def display
		puts "#{@name}-#{@age}-#{@program}-#{@major}"
	end

end

# test = Student.new('wupeixin', 28, 'hw', 'IT')
# test.display


def sort_students(student_array, sym)
	return student_array.sort {|x,y| x.send(sym) <=> y.send(sym) }
end

student_array	=	[	
	Student.new('John',	21,	'MA',	'IT'),	
	Student.new('Ablaham',	22,	'BA',	'Design'),	
	Student.new('Noel',	20,	'MDIV',	'Theology'),	
	Student.new('Peter',	23,	'PHD',	'Ecology'),	
]

# p sort_students(student_array, :name)